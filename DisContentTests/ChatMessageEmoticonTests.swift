import XCTest
import DisContent

class ChatMessageEmoticonTests: XCTestCase {

    func testEmoticonSearch() {
        // Arrange
        for (input, expected) in Fixtures.messageResults {

            // Act
            let actual = input.findEmoticons()

            // Assert
            XCTAssertEqual(expected.emoticons, actual)
        }
    }

    func testEmoticonPerformance() {
        self.measureBlock {
            for _ in 0...1000 {
                self.testEmoticonSearch()
            }
        }
    }
}
