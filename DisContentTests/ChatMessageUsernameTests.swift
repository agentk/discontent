import XCTest
import DisContent

class ChatMessageUsernameTests: XCTestCase {

    func testUsernames() {
        // Arrange
        for (input, expected) in Fixtures.messageResults {

            // Act
            let actual = input.findMentions()

            // Assert
            XCTAssertEqual(expected.mentions, actual)
        }
    }

    func testNonWordUsernameBoundaries() {
        // Arrange
        let (input, expected) = ("@nasir @mr-robot bob@bob.com", ["nasir", "mr"])

        // Act
        let actual = input.findMentions()

        // Assert
        XCTAssertEqual(expected, actual)
    }

    func testUsernamePerformance() {
        self.measureBlock {
            for _ in 0...1000 {
                self.testUsernames()
            }
        }
    }
}

