import XCTest
import DisContent

class ChatMessageLinkTests: XCTestCase {

    func testLinkURLs() {
        // Arrange
        for (input, expected) in Fixtures.messageResults {

            // Act
            let actual = input.findLinks()

            // Assert
            XCTAssertEqual(expected.links.map { $0.url }, actual.map { $0.url })
        }
    }

    func testLinkTitles() {
        // Arrange
        for (input, expected) in Fixtures.messageResults {

            // Act
            let actual = input.findLinks().resolveTitles(titleResolver: Fixtures.titleResolver)

            // Assert
            XCTAssertEqual(expected.links.map { $0.title }, actual.map { $0.title })
        }
    }

    func testLinkURLsPerformance() {
        self.measureBlock {
            for _ in 0...1000 {
                self.testLinkURLs()
            }
        }
    }

    func testLinkTitlesPerformance() {
        self.measureBlock {
            for _ in 0...1000 {
                self.testLinkTitles()
            }
        }
    }
}
