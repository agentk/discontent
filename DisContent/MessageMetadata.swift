public struct MessageMetadata: Equatable {

    public let mentions: [String]
    public let emoticons: [String]
    public let links: [MessageLink]

    public init(mentions: [String] = [], emoticons: [String] = [], links: [MessageLink] = []) {
        self.mentions = mentions
        self.emoticons = emoticons
        self.links = links
    }
}

public func ==(lhs: MessageMetadata, rhs: MessageMetadata) -> Bool {
    return lhs.mentions == rhs.mentions &&
        lhs.emoticons == rhs.emoticons &&
        lhs.links == rhs.links
}
