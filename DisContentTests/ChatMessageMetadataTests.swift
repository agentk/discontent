import XCTest

class ChatMessageMetadataTests: XCTestCase {

    func testMetadata() {
        // Arrange
        for (input, expected) in Fixtures.messageResults {

            // Act
            let actual = input.findMessageMetadata(titleResolver: Fixtures.titleResolver)

            // Assert
            XCTAssertEqual(expected, actual)
        }
    }

    func testMetadataPerformance() {
        self.measureBlock {
            for _ in 0...1000 {
                self.testMetadata()
            }
        }
    }
}
