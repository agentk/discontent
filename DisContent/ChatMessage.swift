import Foundation

public protocol ChatMessage {

    func findMentions() -> [String]
    func findEmoticons() -> [String]
    func findLinks() -> [MessageLink]
    func findMessageMetadata(titleResolver resolver: URLTitleResolver) -> MessageMetadata
}

var emoticonMatcher: NSRegularExpression = {
    guard let matcher = try? NSRegularExpression(pattern: "(?:\\()(\\w+)(?:\\))", options: []) else {
        fatalError("Unable to create emoticonMatcher. Invalid regex pattern")
    }
    return matcher
}()

var mentionMatcher: NSRegularExpression = {
    guard let matcher = try? NSRegularExpression(pattern: "(?<!\\w)(?:@)(\\w+)", options: []) else {
        fatalError("Unable to create mentionMatcher. Invalid regex pattern")
    }
    return matcher
}()

var linkMatcher: NSRegularExpression = {
    guard let matcher = try? NSDataDetector(types: NSTextCheckingType.Link.rawValue) else {
        fatalError("Unable to create linkMatcher. Invalid data detector type")
    }
    return matcher
}()

extension String: ChatMessage {

    func range() -> NSRange {
        return NSRange(location: 0, length: characters.count)
    }

    public func findMentions() -> [String] {
        let nsSelf = self as NSString
        return mentionMatcher.matchesInString(self, options: [], range: range())
            .map { nsSelf.substringWithRange($0.rangeAtIndex(1)) }
    }

    public func findEmoticons() -> [String] {
        let nsSelf = self as NSString
        return emoticonMatcher.matchesInString(self, options: [], range: range())
            .map { nsSelf.substringWithRange($0.rangeAtIndex(1)) }

    }

    public func findLinks() -> [MessageLink] {
        return linkMatcher
            .matchesInString(self, options: [], range: NSRange(location: 0, length: characters.count))
            .flatMap { $0.URL }
            .map { MessageLink(url: $0.absoluteString, title: "") }
    }

    public func findMessageMetadata(titleResolver resolver: URLTitleResolver) -> MessageMetadata {
        return MessageMetadata(
            mentions: findMentions(),
            emoticons: findEmoticons(),
            links: findLinks().resolveTitles(titleResolver: resolver))
    }
}
