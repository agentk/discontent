public typealias URLTitleResolver = (String) -> String?

public struct MessageLink: Equatable {

    public let url: String
    public let title: String

    public init(url: String, title: String) {
        self.url = url
        self.title = title
    }

    public func resolveTitle(titleResolver resolve: URLTitleResolver) -> MessageLink {
        return MessageLink(url: url, title: resolve(url) ?? title)
    }
}


public func ==(lhs: MessageLink, rhs: MessageLink) -> Bool {
    return lhs.url == rhs.url && lhs.title == rhs.title
}

extension SequenceType where Generator.Element == MessageLink {

    public func resolveTitles(titleResolver resolver: URLTitleResolver) -> [MessageLink] {
        return map { $0.resolveTitle(titleResolver: resolver) }
    }
}
