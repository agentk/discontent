import UIKit

class ViewController: UIViewController {


    @IBOutlet var messageField: UITextField!
    @IBOutlet var mentionsLabel: UILabel!
    @IBOutlet var emoticonsLabel: UILabel!
    @IBOutlet var linksLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        messageField.addTarget(self, action: #selector(messageValueDidChange), forControlEvents: UIControlEvents.AllEditingEvents)
        messageValueDidChange()
    }

    func messageValueDidChange() {
        let text = messageField.text ?? ""
        let metadata = text.findMessageMetadata { url in
            return "Unknown"
        }
        mentionsLabel.text = metadata.mentions.joinWithSeparator(", ")
        emoticonsLabel.text = metadata.emoticons.joinWithSeparator(", ")
        linksLabel.text = metadata.links
            .map { "[\($0.title)](\($0.url))" }
            .joinWithSeparator(", ")
    }
}
