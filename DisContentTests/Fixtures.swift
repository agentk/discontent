import DisContent

struct Fixtures {
    static let messageResults = [
        ("@chris you around?",
            MessageMetadata(
                mentions: ["chris"])),

        ("Good morning! (megusta) (coffee)",
            MessageMetadata(
                emoticons: ["megusta", "coffee"])),

        ("Olympics are starting soon;http://www.nbcolympics.com",
            MessageMetadata(
                links: [MessageLink(
                    url: "http://www.nbcolympics.com",
                    title: "NBC Olympics | 2014 NBC Olympics in Sochi Russia")])),

        ("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
            MessageMetadata(
                mentions: ["bob", "john"],
                emoticons: ["success"],
                links: [MessageLink(
                    url: "https://twitter.com/jdorfman/status/430511497475670016",
                    title: "Twitter / jdorfman: nice @littlebigdetail from ...")])),
        ]

    static let titleCache = [
        "http://www.nbcolympics.com": "NBC Olympics | 2014 NBC Olympics in Sochi Russia",
        "https://twitter.com/jdorfman/status/430511497475670016": "Twitter / jdorfman: nice @littlebigdetail from ...",
    ]

    static func titleResolver(url: String) -> String? {
        return titleCache[url]
    }
}
